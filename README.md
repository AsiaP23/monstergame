# Pierwsza gra #

Pierwsza gra napisana w języku Java.

### Opis ###

Gra polega na omijaniu wrogów, które poruszają się chaotycznie.

Klawisze **←↑→↓** umożliwiają sterowanie postacią gracza, natomiast klawisz **WSAD** umożliwiają strzelanie w potworki w odpowiednich kierunkach.
Niebieskie potworki umierają po jednym strzale, natomiast pomarańczowe potworki po dwóch strzałach. Zastrzelenie  niebieskiego potworka nagradzane jest 10 puntami, natomiast pomarańczowego 20 punktami. Każdy strzał kosztuje gracza 1 punkt. Każdy nowy poziom gry nagradzany jest 100 punktami.
Na początku gry gracz posiada trzy życia, każde zderzenie z potworkiem kosztuje go punkty życia. Co pewien czas pojawiają sie na planszy czerwone serduszka, które po złapaniu przez gracza, dodają mu dodatkowy poziom życia.

![gra.png](https://bitbucket.org/repo/RBK55a/images/2045710528-gra.png)