package game.one;

import java.util.Random;

public class Spawn {

	private Handler handler;
	private HealthBarStatus hbs;
	private Random r;

	
	private int scoreKeep = 0;
	
	public Spawn(Handler handler, HealthBarStatus hbs ) {
		this.handler = handler;
		this.hbs = hbs;
	}
	
	public void tick(){
		scoreKeep++;
		
		if (scoreKeep >= 1000){
			scoreKeep = 0; 
			hbs.setLevel(hbs.getLevel() + 1);
			
//			handler.addObject(new Player(r.nextInt(Game.WIDTH / 2 - 10), r.nextInt(Game.HEIGHT / 2 - 10), ID.Player, handler));
			
			handler.addObject(new Enemy(Game.WIDTH / 2 - 100, Game.HEIGHT / 2-100, ID.Enemy, handler));
		}
	}
}
