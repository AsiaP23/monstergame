package game.one;

import java.util.Random;

public class Logic {

	private Handler handler;
	private HealthBarStatus hbs;
	private Random r = new Random();

	private int scoreKeep = 20;
	private int liveKeep = 3;
	private int counter = 0;
	private boolean isAlive = true;
	private int scoreHandler = 0;

	public Logic(Handler handler, HealthBarStatus hbs) {
		this.handler = handler;
		this.hbs = hbs;
	}

	public void tick() {
		hbs.setLive(liveKeep);
		if (liveKeep == 0)
			isAlive = false;

		if (isAlive) {
			counter++;
			scoreHandler = scoreKeep;
//			if (counter % 50 == 0) {
//				scoreKeep++;
//				hbs.setScore(scoreKeep);
//			}
			if (KeyInput.bulletFlag && scoreKeep > 0) {
				scoreKeep--;
				hbs.setScore(scoreKeep);
				KeyInput.bulletFlag = false;
			}
			
			if (Bullet.enemyHitFlag)  {
				scoreKeep += 10;
				hbs.setScore(scoreKeep);
				Bullet.enemyHitFlag = false;
			}
			if (Bullet.monsterHitFlag == 2)  {
				scoreKeep += 20;
				hbs.setScore(scoreKeep);
				Bullet.monsterHitFlag = 0;
			}

			if (counter % 200 == 0) {
				handler.addObject(new Enemy(Game.WIDTH / 2 - 100, Game.HEIGHT / 2 - 100, ID.Enemy, handler));
			}
			if (counter % 400 == 0 ) {
				handler.addObject(new Monster(Game.WIDTH / 2 + 100, Game.WIDTH / 2 + 100, ID.Monster, handler));
			}
			if (counter % 1000 == 0 ) {
				handler.addObject(new Life(r.nextInt((590 - 50) + 1) + 50, 10, ID.Life, handler));
			}
			if (counter % 1000 == 0){
				hbs.setLevel(hbs.getLevel() + 1);
				scoreKeep += 100;
				scoreHandler = 0;
			}
			if (HealthBarStatus.HEALTH == 0) {
				liveKeep--;
				hbs.setLive(hbs.getLive() - 1);
				HealthBarStatus.HEALTH = 100;
			}
			if (Life.lifeFlag) {
				Life.lifeFlag = false;
				liveKeep++;
				hbs.setLive(hbs.getLive() + 1);
				HealthBarStatus.HEALTH = 100;
				
			}
		}
		if (!isAlive) {
			HealthBarStatus.HEALTH = 0;
			for (int i = 0; i < handler.object.size(); i++) {
				GameObject tempObject = handler.object.get(i);
				handler.removeObject(tempObject);
			}
			handler.addObject(new GameOver(10, 0, 5, ID.GameOver));
		}
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
	
}
