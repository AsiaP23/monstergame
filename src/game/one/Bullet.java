package game.one;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Bullet extends GameObject{
	public Handler handler;
	public Random r = new Random();
	public static boolean enemyHitFlag = false;
	public static int monsterHitFlag = 0;

	public Bullet(int x, int y, ID id,  Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		velX = r.nextInt((7 - 3) + 1) + 3;
		velY = r.nextInt((7 - 3) + 1) + 3;
	}

	public void tick() {
		x += velX;
		y += velY;
		
		collisionBullet();
	}

	public void render(Graphics g) {
		LoadImageApp imgBullet = new LoadImageApp("Bullet.png");
		imgBullet.paint(g, x, y);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, 20, 20);
	}
	
	private void collisionBullet(){
		for (int i = 0; i < handler.object.size(); i++){
			GameObject tempObject = handler.object.get(i);
			if(tempObject.getId() == ID.Enemy){
				if(getBounds().intersects(tempObject.getBounds())){
					handler.removeObject(tempObject);
					handler.removeObject(this);
					enemyHitFlag = true;
				}
			}
			if(tempObject.getId() == ID.Monster){
				if(getBounds().intersects(tempObject.getBounds())){
					monsterHitFlag++;
					handler.removeObject(this);
					if (monsterHitFlag == 2)
					handler.removeObject(tempObject);
					handler.removeObject(this);
				}
			}
		}
	}

}
