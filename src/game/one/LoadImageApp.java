package game.one;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

public class LoadImageApp {

	public String typeOfObject;
	BufferedImage img;

    public String getTypeOfObject() {
		return typeOfObject;
	}

	public void setTypeOfObject(String typeOfObject) {
		this.typeOfObject = typeOfObject;
	}

	public void paint(Graphics g, int x, int y) {
        g.drawImage(img, x, y, null);
    }

    public LoadImageApp(String typeOfObject) {
    	this.typeOfObject = typeOfObject;
       try {
           img = ImageIO.read(new File(this.typeOfObject + ""));
       } catch (IOException e) {
       }
    }

    public Dimension getPreferredSize() {
        if (img == null) {
             return new Dimension(100,100);
        } else {
           return new Dimension(img.getWidth(null), img.getHeight(null));
       }
    }
}
